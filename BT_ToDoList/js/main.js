let taskList = [];
let completeTaskList = [];
let sortTimeDown = false;
const MONTH_NAMES = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

//---Add new task event, when add button is clicked
document.getElementById("addItem").addEventListener("click", () => {
  let task = document.getElementById("newTask").value;
  if (task) {
    addNewTask(task);
  }
  document.getElementById("newTask").value = "";
});
//---Render To Do or Completed task
const renderTask = (section, taskList) => {
  let contentHTML = "";
  let currentDate = "";
  taskList.forEach((task) => {
    let content = "";
    if (task.date != currentDate) {
      content += /*html*/ `
      <div class="card__title">
          <p>${task.date}</p>
     </div>`;
    }
    content += /*html*/ `
        <li>
            <span>${task.taskName}</span>
            <div class="buttons">
                <button class="remove" onclick="deleteTask('${task.taskName}')">
                    <i class="fa fa-trash-alt"></i>
                </button>
                <button class="complete" onclick="completeTask('${task.taskName}')">
                <i class="fas fa-check-circle"></i>
                <i class="far fa-check-circle"></i>
                </button>
            </div>
        </li>
        `;
    contentHTML += content;
    currentDate = task.date;
  });
  document.getElementById(section).innerHTML = contentHTML;
};
//---Show all complete task--//
//---Sort date---///
document.getElementById("all").addEventListener("click", () => {
  sortTimeDown = !sortTimeDown;
  if (sortTimeDown) {
    if (taskList != "") {
      taskList.sort((a, b) => {
        return a.date < b.date;
      });
      renderTask("todo", taskList);
    }
  } else {
    if (taskList != "") {
      taskList.sort((a, b) => {
        return a.date > b.date;
      });
      renderTask("todo", taskList);
    }
  }
});
//----Sort down event
document.getElementById("two").addEventListener("click", () => {
  if (taskList != "") {
    taskList.sort();
    renderTask("todo", taskList);
  }
});
//-----Sort up event
document.getElementById("three").addEventListener("click", () => {
  if (taskList != "") {
    taskList.sort((a, b) => {
      return a < b;
    });
    renderTask("todo", taskList);
  }
});
//----Add new task
const addNewTask = (task) => {
  let date = new Date();
  let taskDate =
    MONTH_NAMES[date.getMonth()] +
    " " +
    date.getDate() +
    "," +
    date.getFullYear();
  let obj = { taskName: task, date: taskDate };
  taskList.push(obj); //Add new task to array
  renderTask("todo", taskList);
};

//----Delete task event
const deleteTask = (task) => {
  //----Find task index in TaskList array
  let taskIndex = taskList.findIndex((item) => {
    return item.taskName == task;
  });
  //----Check delete task in To Do or Completed section
  if (taskIndex >= 0) {
    let parentNode = document.getElementById("todo");
    let childNode = parentNode.getElementsByTagName("li"); ///Get all li node in To Do
    taskList.splice(taskIndex, 1);
    if (childNode[taskIndex].previousElementSibling.tagName == "DIV") {
      childNode[taskIndex].previousElementSibling.remove();
    }
    childNode[taskIndex].remove();
    renderTask("todo", taskList);
  } else {
    let completeTaskIndex = completeTaskList.findIndex((item) => {
      return item.taskName == task;
    });
    let parentNode = document.getElementById("completed");
    let childNode = parentNode.getElementsByTagName("li"); ///Get all li node in Completed
    completeTaskList.splice(completeTaskIndex, 1);
    if (childNode[completeTaskIndex].previousElementSibling.tagName == "DIV") {
      childNode[completeTaskIndex].previousElementSibling.remove();
    }
    childNode[completeTaskIndex].remove();
    renderTask("completed", completeTaskList);
  }
};
///-----Add task to Complete task section
const completeTask = (task) => {
  let taskIndex = taskList.findIndex((item) => {
    return item.taskName == task;
  });
  let parentNode = document.getElementById("todo");
  let childNode = parentNode.getElementsByTagName("li");
  completeTaskList.push(taskList[taskIndex]);
  completeTaskList.sort((a, b) => {
    return a.date > b.date;
  });
  taskList.splice(taskIndex, 1);
  if (childNode[taskIndex].previousElementSibling.tagName == "DIV") {
    childNode[taskIndex].previousElementSibling.remove();
  }
  childNode[taskIndex].remove();
  completeTaskList.sort((a, b) => {
    return a.date > b.date;
  });
  renderTask("completed", completeTaskList);
  renderTask("todo", taskList);
};
//----Get current date----//
