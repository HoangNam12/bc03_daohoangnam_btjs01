/**
 * Bài 1: Quản lý tuyển sinh
 */
function finalResult() {
  var referencePoint = document.getElementById("referencePoint").value * 1;
  var result = validationPoint(
    referencePoint,
    calculateBonusPoint(),
    calculatePoint()
  );
  if (result >= 0) {
    document
      .getElementById("printResult")
      .setAttribute("class", "text-success");
    document.getElementById(
      "printResult"
    ).innerHTML = `<p> Bạn đã trúng tuyển</p>`;
  } else {
    document.getElementById("printResult").setAttribute("class", "text-danger");
    document.getElementById("printResult").innerHTML = `<p> Bạn đã rớt</p>`;
  }
}
var calculatePoint = function () {
  var firstPoint = document.getElementById("firstPoint").value * 1;
  var secondPoint = document.getElementById("secondPoint").value * 1;
  var thirdPoint = document.getElementById("thirdPoint").value * 1;
  if (firstPoint * secondPoint * thirdPoint != 0) {
    return firstPoint + secondPoint + thirdPoint;
  } else {
    return 0;
  }
};
var calculateBonusPoint = function () {
  var locationPoint = document.getElementById("location");
  var priorityPoint = document.getElementById("priority");
  var bonusPoint =
    priorityPoint.options[priorityPoint.selectedIndex].value * 1 +
    locationPoint.options[locationPoint.selectedIndex].value * 1;
  return bonusPoint;
};
var validationPoint = function (referencePoint, bonusPoint, sumPoint) {
  return sumPoint + bonusPoint - referencePoint;
};
/**
 *
 * Bài 2
 *
 */

function finalResult2() {
  var customerName = document.getElementById("name").value;
  var energyConsume = document.getElementById("kwConsume").value * 1;
  var totalMoney = calculateMoney(energyConsume);
  document.getElementById(
    "printResult2"
  ).innerHTML = `<p> Khách hàng ${customerName} phải trả ${Number(
    totalMoney
  ).toLocaleString()}</p>`;
}
var calculateMoney = function (energy) {
  var arr = [
    [0, 50, 100, 200, 350],
    [0, 25000, 57500, 142500, 307500],
  ];
  var moneyCal = 0;
  for (var i = 1; i < arr[1].length; i++) {
    if (energy > 350) {
      moneyCal = (energy - 350) * 1300 + arr[1][arr[1].length - 1];
    } else if (energy < arr[0][i]) {
      moneyCal =
        ((energy - arr[0][i - 1]) * (arr[1][i] - arr[1][i - 1])) /
          (arr[0][i] - arr[0][i - 1]) +
        arr[1][i - 1];
      break;
    }
  }
  return moneyCal;
};
/**
 *
 * Bài 3
 *
 */
function finalResult3() {
  var customerName = document.getElementById("username").value;
  var totalIncome = document.getElementById("yearIncome").value * 1;
  var taxPerson = document.getElementById("dependentPerson").value * 1;
  var taxIncome = totalIncome - 4000000 - taxPerson * 1600000;
  var totalTaxMoney = calculateTaxMoney(taxIncome);
  document.getElementById(
    "printResult3"
  ).innerHTML = `<p>Khách hàng ${customerName} phải đóng ${Number(
    totalTaxMoney
  ).toLocaleString()} VND tiền thuế </p>`;
}
var calculateTaxMoney = function (money) {
  var arrTax = [
    0, 60000000, 120000000, 210000000, 384000000, 624000000, 960000000,
  ];
  var taxMoney = 0;
  for (var i = 1; i < arrTax.length; i++) {
    if (money > arrTax[arrTax.length - 1]) {
      taxMoney = (money * 35) / 100;
    } else if (money <= arrTax[i]) {
      taxMoney = (money * 5 * i) / 100;
      break;
    }
  }
  return taxMoney;
};
/**
 *
 * Bài 4
 *
 */

function finalResult4() {
  var customerType = document.getElementById("customer").value;
  var customerId = document.getElementById("customerId").value;
  var premiumRentChannel = document.getElementById("premiumChannel").value * 1;
  document.getElementById(
    "printResult4"
  ).innerHTML = `<p> Mã khách hàng: ${customerId} - Tiền cáp: ${calculateMoneyChannel(
    customerType,
    premiumRentChannel
  )}$</p>`;
}
function addPort() {
  var customerType = document.getElementById("customer").value;
  if (customerType == 2) {
    document.getElementById("port").style.display = "block";
  } else {
    document.getElementById("port").style.display = "none";
  }
}
var calculateMoneyChannel = function (customer, premiumChannel) {
  var moneyRent = 0;
  var connect = document.getElementById("connectPort").value * 1;
  if (customer == 1) {
    moneyRent = 4.5 + 20.5 + 7.5 * premiumChannel;
  } else if (customer == 2) {
    var bonusMoney = connect > 10 ? 75 + 5 * (connect - 10) : 75;
    moneyRent = 15 + bonusMoney + 50 * premiumChannel;
  }
  return moneyRent;
};
