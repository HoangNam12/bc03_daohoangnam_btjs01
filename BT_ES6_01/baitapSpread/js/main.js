let jumpHoverAnimation = (string) => {
    let chars = [...string];
    let contentHTML = "";
    chars.forEach((char) => {
        let content = "";
        content = `<span>${char}</span>`;
        contentHTML += content;
    })
    document.querySelector(".heading").innerHTML = contentHTML;
}
let string = document.querySelector(".heading").innerHTML;
jumpHoverAnimation(string);