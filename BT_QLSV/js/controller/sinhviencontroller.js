//--Lay thong tin tu form
function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var emailSv = document.getElementById("txtEmail").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemLy = document.getElementById("txtDiemLy").value * 1;
  var diemHoa = document.getElementById("txtDiemHoa").value * 1;
  return new SinhVien(maSv, tenSv, emailSv, diemToan, diemLy, diemHoa);
}
//--Hien thi danh sach Sinh vien
function xuatDanhSachSinhVien(dssv) {
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var sinhVien = dssv[i];
    var contentTrTag = `<tr>
      <td>${sinhVien.maSv}</td>
      <td>${sinhVien.tenSv}</td>
      <td>${sinhVien.emailSv}</td>
      <td>${sinhVien.diemTrungBinh()}</td>
      <td>
        <button class="btn btn-success" onclick="suaSinhVien('${sinhVien.maSv
      }')">Sửa</button>
        <button class="btn btn-danger" onclick="xoaSinhVien('${sinhVien.maSv
      }')">Xoá</button>
      </td>
      </tr>`;
    contentHTML += contentTrTag;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
//----Hien thi thong tin len form
function xuatThongTinForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSv;
  document.getElementById("txtTenSV").value = sv.tenSv;
  document.getElementById("txtEmail").value = sv.emailSv;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
