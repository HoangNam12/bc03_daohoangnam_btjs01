function ValidationSV() {
  //---Kiem tra thong tin co rong
  this.kiemtraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (valueTarget == "") {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  //---Kiem tra id co bi trung
  this.kiemTraIdHopLe = function (newSinhVien, danhSachSinhVien) {
    var index = danhSachSinhVien.findIndex(function (item) {
      return item.maSv == newSinhVien.maSv;
    });
    if (index == -1) {
      document.getElementById("spanMaSV").innerHTML = "";
      return true;
    }
    document.getElementById("spanMaSV").innerHTML =
      "Mã sinh viên không được trùng";
    return false;
  };
  //---Kiem tra email co dung format
  this.kiemTraEmail = function (idTarget, idError) {
    var pattern =
      /^[a-zA-Z0-9](\.?[a-zA-Z0-9]){5,}\@([a-zA-Z0-9]+)([\.])([a-zA-Z\.]+)/g;
    var valueInput = document.getElementById(idTarget).value;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    var strLenght = valueInput.indexOf("@");
    if (strLenght < 6) {
      document.getElementById(idError).innerText =
        "Email phải có ít nhất 6 kí tự";
      return false;
    }
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  };
}
