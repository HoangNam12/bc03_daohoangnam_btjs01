import logo from "./logo.svg";
import "./App.css";
import TryGlass from "./virtual_glasses/TryGlass";

function App() {
  return (
    <div className="App">
      <TryGlass />
    </div>
  );
}

export default App;
