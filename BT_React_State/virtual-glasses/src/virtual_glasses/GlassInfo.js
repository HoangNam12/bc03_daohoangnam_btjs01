import React, { Component } from "react";

export default class GlassInfo extends Component {
  render() {
    let glass = this.props.glassInfo;
    return (
      <div className="text-left">
        <div id="glass-brand">{glass.name}</div>
        <div id="glass-status">
          <div id="glass-price">${glass.price}</div>
          <span class="text-success">stocking</span>
        </div>
        <div id="glass-description">{glass.desc}</div>
      </div>
    );
  }
}
