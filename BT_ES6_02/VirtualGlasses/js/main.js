let glassList = [];
fetch("./data/data.txt")
  .then((response) => {
    return response.text();
  })
  .then((data) => {
    glassList = convertToObject(data, glassList);
    renderGlassList();
  })
  .catch((err) => {
    console.log(err);
  });
//----Show list glasses on left side
const renderGlassList = () => {
  let contentHTML = "";
  glassList.forEach((glass) => {
    let content = "";
    content = `
      <img class="col-4" src="${glass.src}" id="${glass.id}" onclick="showVirtualGlass('${glass.id}')"></img>
      `;
    contentHTML += content;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

//---Convert text file to object
const convertToObject = (text, array) => {
  do {
    var startBracket = text.indexOf("{");
    var endBracket = text.indexOf("}");
    let item = text.substring(startBracket + 1, endBracket); //get data inside {}
    if (item != "") {
      let obj = eval("({" + item + "})"); //convert to object
      text = text.replace(`{${item}}`, ""); //add to array
      array.push(obj);
    }
  } while (startBracket > -1);
  return array;
};
//----Show virtual glass on model
const showVirtualGlass = (glassImgID) => {
  let glassIndex = glassList.findIndex((glass) => {
    return glass.id == glassImgID;
  });
  let contentHTML = `
      <img id="virtualGlass" src="${glassList[glassIndex].virtualImg}"></img>
      `;
  let contentInfo = `
  <div id="glass-brand">${glassList[glassIndex].name} - ${glassList[glassIndex].brand} (${glassList[glassIndex].color})</div>
  <div id="glass-status">
      <div id="glass-price">$${glassList[glassIndex].price}</div>
      <span class="text-success">stocking</span>
  </div>
  <div id="glass-description">${glassList[glassIndex].description}</div>
  `;
  document.getElementById("avatar").innerHTML = contentHTML;
  document.getElementById("glassesInfo").innerHTML = contentInfo;
  animateGlassInfo();
};
//---Animation for Glass info
const animateGlassInfo = () => {
  var id = null;
  var element = document.getElementById("glassesInfo");
  var position = 0;
  clearInterval(id);
  id = setInterval(frame, 10);
  function frame() {
    if (position == 150) {
      clearInterval(id);
    } else {
      position++;
      element.style.top = -position + "px";
    }
  }
};
//----Show and hide virtual glass
const removeGlasses = function (action) {
  if (!action) {
    document.getElementById("virtualGlass").style.display = "none";
  } else {
    document.getElementById("virtualGlass").style.display = "block";
  }
};
window.removeGlasses = removeGlasses;
window.showVirtualGlass = showVirtualGlass;
