const usernamePattern = /^[a-zA-Z0-9]{1,}$/;
const namePattern = /^[a-zA-Z\s]{1,}$/;
const emailPattern =
  /^([a-zA-Z0-9]+)([_.-])?([a-zA-Z0-9]+)@([a-zA-Z]+)([.])([a-zA-Z.]+)$/;
const passwordPattern =
  /^(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&])[A-Za-z0-9@$!%*?&]{6,10}$/;

const MAXIMUM_DESCRIPTION_LENGTH = 60;
// Xoa hien thi loi
const clearSpanError = function (element) {
  document.getElementById(element).style.display = "none";
  document.getElementById(element).innerHTML = "";
};
//  Hien thi loi
const showSpanError = function (element, message) {
  document.getElementById(element).style.display = "block";
  document.getElementById(element).innerHTML = message;
};
const getUserIndex = (username, list) => {
  return list.findIndex((user) => {
    return user.username == username;
  });
};
// Kiem tra input rong
const checkEmpty = function (info, element, elementName) {
  if (info === "") {
    var msg = `${elementName} can not be empty`;
    showSpanError(element, msg);
    return false;
  }
  clearSpanError(element);
  return true;
};
// Kiem tra input co dung pattern
const checkValid = function (info, element, elementName) {
  var pattern = eval(`${elementName + "Pattern"}`);
  var msg;
  if (info.match(pattern)) {
    clearSpanError(element);
    return true;
  } else {
    if (elementName == "username") {
      msg = `${elementName} has only string and number`;
    } else if (elementName == "name") {
      msg = `${elementName} must be string`;
    } else if (elementName == "email") {
      msg = `${elementName} is invalid`;
    } else if (elementName == "password") {
      msg = `${elementName} has at least 1 number, 1 uppercase character, 1 special characters`;
    }
    showSpanError(element, msg);
    return false;
  }
};
// Kiem tra input co trung
const checkDuplicate = function (index, element, elementName) {
  if (index != -1) {
    var msg = `The ${elementName} is duplicated`;
    showSpanError(element, msg);
    return false;
  }
  clearSpanError(element);
  return true;
};
// Kiem tra username : Not empty , Not duplicated, must be string, number
const validationUserName = function (username, list) {
  var isValid =
    checkEmpty(username, "tbTaiKhoan", "username") &&
    checkValid(username, "tbTaiKhoan", "username") &&
    checkDuplicate(getUserIndex(username, list), "tbTaiKhoan", "username");
  return isValid;
};
// Kiem tra name: not empty, must be string
const validationName = (name) => {
  var isValid =
    checkEmpty(name, "tbHoTen", "name") && checkValid(name, "tbHoTen", "name");
  return isValid;
};
// Kiem tra email: not empty, right email format(ex: abc@example.com)
const validationEmail = (email) => {
  var isValid =
    checkEmpty(email, "tbEmail", "email") &&
    checkValid(email, "tbEmail", "email");
  return isValid;
};
// Kiem tra password: not empty, right format(1 upper case, 1 special character, 1 number, length = 6-8)
const validationPassword = (password) => {
  var isValid =
    checkEmpty(password, "tbMatKhau", "password") &&
    checkValid(password, "tbMatKhau", "password");
  return isValid;
};
// Kiem tra hinh anh: not empty
const validationImage = (imageUrl) => {
  var isValid = checkEmpty(imageUrl, "tbHinhAnh", "imageUrl");
  return isValid;
};
// Kiem tra loai nguoi dung: not empty
const validationType = (type) => {
  var isValid = checkEmpty(type, "tbLoaiNguoiDung", "nguoi dung");
  return isValid;
};
// Kiem tra ngon ngu: not empty
const validationLanguage = (language) => {
  var isValid = checkEmpty(language, "tbLoaiNgonNgu", "ngon ngu");
  return isValid;
};
// Kiem tra mo ta: not empty, length < 60 characters
const validationDescription = (description) => {
  var isValid = checkEmpty(description, "tbMoTa", "mo ta");
  if (description.length > MAXIMUM_DESCRIPTION_LENGTH) {
    showSpanError("tbMoTa", "The description must be less than 60 characters");
    isValid = false;
    return isValid;
  }
  return isValid;
};
// Kiem tra khi them moi nhan vien
const validationUser = function (user, list) {
  return (
    validationUserName(user.username, list) &
    validationName(user.name) &
    validationEmail(user.email) &
    validationPassword(user.password) &
    validationImage(user.imageUrl) &
    validationType(user.type) &
    validationLanguage(user.language) &
    validationDescription(user.description)
  );
};
// Kiem tra khi cap nhat thong tin nhan vien
const validationUpdateUser = function (user) {
  return (
    validationName(user.name) &
    validationEmail(user.email) &
    validationPassword(user.password) &
    validationImage(user.imageUrl) &
    validationType(user.type) &
    validationLanguage(user.language) &
    validationDescription(user.description)
  );
};
export { validationUser, validationUpdateUser, clearSpanError };
