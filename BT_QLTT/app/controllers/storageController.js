import { Teacher } from "../models/userModel.js";

const USERLIST_LOCAL_STORAGE = "USERLIST_LOCAL_STORAGE";

//--- Save input data to local storage---///
const saveLocalStorage = function (list) {
  var userListJson = JSON.stringify(list);
  localStorage.setItem(USERLIST_LOCAL_STORAGE, userListJson);
};
//--- Get data from local storage for quick validation---//
const getLocalStorage = function () {
  let list = localStorage.getItem(USERLIST_LOCAL_STORAGE);
  let userList = [];
  if (list) {
    userList = JSON.parse(list);
    userList = userList.map(function (user) {
      return new Teacher(
        user.id,
        user.username,
        user.name,
        user.password,
        user.email,
        user.type,
        user.language,
        user.description,
        user.imageUrl
      );
    });
  }
  return userList;
};
export { getLocalStorage, saveLocalStorage };
