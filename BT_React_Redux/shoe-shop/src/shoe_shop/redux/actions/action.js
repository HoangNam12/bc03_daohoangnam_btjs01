import { ADD_TO_CART } from "../constant/constant";

export const addProduct = (product) => {
  return {
    type: ADD_TO_CART,
    payload: product,
  };
};
