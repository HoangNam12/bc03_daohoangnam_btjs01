export const ADD_TO_CART = "Add to cart";
export const DELETE_PRODUCT = "Delete product";
export const SHOW_PRODUCT_DETAIL = "Show product detail";
export const HANDLE_PRODUCT_NUMBER = "Handel product number";
