import React, { Component } from "react";
import ProductItem from "./ProductItem";
import { connect } from "react-redux";
class ProductList extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            {this.props.productData?.map((product) => {
              return <ProductItem product={product} />;
            })}
          </div>
        </div>
      </div>
    );
  }
}
let mapsStateToProps = (state) => {
  return {
    productData: state.shoeShopReducer.productList,
  };
};

export default connect(mapsStateToProps, null)(ProductList);
