import React, { Component } from "react";
import ProductList from "./ProductList";
import ShoesCart from "./ShoesCart";
import ProductDetail from "./ProductDetail";
import { connect } from "react-redux";
class ShoesStore extends Component {
  render() {
    let tongSoLuong = this.props.cart?.reduce((tong, product) => {
      return (tong += product.numberInCart);
    }, 0);
    return (
      <div className="container">
        <h1>SHOE SHOP</h1>
        <div className="text-right">
          <span
            className="text-danger"
            style={{ cursor: "pointer", fontSize: "17px", fontWeight: "bold" }}
            data-toggle="modal"
            data-target="#cartModal"
          >
            <i class="fa fa-shopping-cart"></i>({tongSoLuong})
          </span>
        </div>
        <ProductList />
        <ShoesCart />
        <ProductDetail />
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.cart,
  };
};
export default connect(mapStateToProps)(ShoesStore);
