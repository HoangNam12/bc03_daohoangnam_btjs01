const accountPattern = /^[0-9]{4,6}$/;
const namePattern = /^[a-zA-Z\s]{1,}$/;
const emailPattern =
  /^([a-zA-Z0-9]+)([_.-])?([a-zA-Z0-9]+)@([a-zA-Z]+)([.])([a-zA-Z.]+)$/;
const passwordPattern =
  /^(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&])[A-Za-z0-9@$!%*?&]{6,10}$/;

const MINIMUM_BASE_SALARY = 1000000;
const MAXIMUM_BASE_SALARY = 20000000;
const MINIMUM_WORK_HOUR = 80;
const MAXIMUM_WORK_HOUR = 200;
// Xoa hien thi loi
const clearSpanError = function (element) {
  document.getElementById(element).style.display = "none";
  document.getElementById(element).innerHTML = "";
};
//  Hien thi loi
const showSpanError = function (element, message) {
  document.getElementById(element).style.display = "block";
  document.getElementById(element).innerHTML = message;
};
// Kiem tra input rong
const checkEmpty = function (info, element, elementName) {
  if (info == "") {
    var msg = `The ${elementName} can not be empty`;
    showSpanError(element, msg);
    return false;
  }
  clearSpanError(element);
  return true;
};
// Kiem tra input co dung pattern
const checkValid = function (info, element, elementName) {
  var pattern = eval(`${elementName + "Pattern"}`);
  var msg;
  if (info.match(pattern)) {
    clearSpanError(element);
    return true;
  } else {
    if (elementName == "account") {
      msg = `The ${elementName} must be 4-6 numbers`;
    } else if (elementName == "name") {
      msg = `The ${elementName} must be string`;
    } else if (elementName == "email") {
      msg = `The ${elementName} is invalid`;
    } else if (elementName == "password") {
      msg = `The ${elementName} has at least 1 number, 1 uppercase character, 1 special characters`;
    }
    showSpanError(element, msg);
    return false;
  }
};
// Kiem tra input co trung
const checkDuplicate = function (index, element, elementName) {
  if (index != -1) {
    var msg = `The ${elementName} is duplicated`;
    showSpanError(element, msg);
    return false;
  }
  clearSpanError(element);
  return true;
};
// Kiem tra account: not empty, length= 4-6 characters, not duplicated
const validationAccount = function (account, list) {
  var isValid =
    checkEmpty(account, "tbTKNV", "account") &&
    checkValid(account, "tbTKNV", "account") &&
    checkDuplicate(getStaffIndex(0, account, list), "tbTKNV", "account");
  return isValid;
};
// Kiem tra name: not empty, must be string
const validationName = function (name) {
  var isValid =
    checkEmpty(name, "tbTen", "name") && checkValid(name, "tbTen", "name");
  return isValid;
};
// Kiem tra email: not empty, right email format (ex: abc@example)
const validationEmail = function (email) {
  var isValid =
    checkEmpty(email, "tbEmail", "email") &&
    checkValid(email, "tbEmail", "email");
  return isValid;
};
// Kiem tra password: not empty, right format (length= 6-10 characters, 1 uppercase, 1 number)
const validationPassword = function (password) {
  var isValid =
    checkEmpty(password, "tbMatKhau", "password") &&
    checkValid(password, "tbMatKhau", "password");
  return isValid;
};
// Kiem tra ngay thang: not empty, right format(mm/dd/yyyy)
const validationDate = function (date) {
  var isFormat = true;
  var isValid = checkEmpty(date, "tbNgay", "work date");
  if (isValid && isNaN(Date.parse(date))) {
    var msg = "The work day is invalid. the day format: mm/dd/yyyy";
    showSpanError("tbNgay", msg);
    isFormat = false;
    return isFormat;
  }
  return isValid;
};
// Kiem tra luong: not empty, 1 000 000 <= base money <= 20 000 000
const validationBaseSalary = function (salary) {
  var isInRange = true;
  var isValid = checkEmpty(salary, "tbLuongCB", "salary");
  if (isValid && (salary < MINIMUM_BASE_SALARY || salary > MAXIMUM_BASE_SALARY)) {
    var msg = "The base salary is out of range [1m - 20m]";
    showSpanError("tbLuongCB", msg);
    isInRange = false;
    return isInRange;
  }
  return isValid;
};
// Kiem tra chuc vu: not empty, must choose value
const validationPosition = function (position) {
  var isValid = true;
  // position =  1 or 2 or 3
  if (isNaN(position)) {
    document.getElementById("tbChucVu").style.display = "block";
    document.getElementById("tbChucVu").innerHTML = "Choose position";
    isValid = false;
  } else {
    document.getElementById("tbChucVu").style.display = "none";
    isValid = true;
  }
  return isValid;
};
// Kiem tra gio lam viec: not empty, 80 <= work hour <=200
const validationWorkHour = function (workHour) {
  var isInRange = true;
  var isValid = checkEmpty(workHour, "tbGiolam", "work hour");
  if (isValid && (workHour < MINIMUM_WORK_HOUR || workHour > MAXIMUM_WORK_HOUR)) {
    var msg = "The work day is out of range [80h - 200h]";
    showSpanError("tbGiolam", msg);
    isInRange = false;
    return isInRange;
  }
  return isValid;
};
// Kiem tra khi them moi nhan vien
const validationStaff = function (staff, list) {
  return (
    validationAccount(staff.account, list) &
    validationName(staff.name) &
    validationEmail(staff.email) &
    validationPassword(staff.password) &
    validationDate(staff.workDay) &
    validationBaseSalary(staff.baseSalary) &
    validationPosition(staff.position) &
    validationWorkHour(staff.workHour)
  );
};
// Kiem tra khi cap nhat nhan vien
const validationUpdateStaff = function (staff) {
  return (
    validationName(staff.name) &
    validationEmail(staff.email) &
    validationPassword(staff.password) &
    validationDate(staff.workDay) &
    validationBaseSalary(staff.baseSalary) &
    validationPosition(staff.position) &
    validationWorkHour(staff.workHour)
  );
};
