var staffList = [];
const STAFFLIST_LOCAL_STORAGE = "STAFFLIST_LOCAL_STORAGE";
const MODE = 0;
// Hien thi danh sach sinh vien
getLocalStorage();
showStaff(staffList);
// Reset form cap nhat
document.getElementById("btnDong").addEventListener("click", function () {
  document.getElementById("body-form").reset();
  document.getElementById("tknv").readOnly = false;
  document.getElementById("btnThemNV").style.display = "block";
  document.getElementById("btnCapNhat").style.display = "block";
  clearSpanError("tbTKNV");
  clearSpanError("tbTen");
  clearSpanError("tbEmail");
  clearSpanError("tbMatKhau");
  clearSpanError("tbNgay");
  clearSpanError("tbLuongCB");
  clearSpanError("tbChucVu");
  clearSpanError("tbGiolam");
});

//Hide cap nhat btn khi them moi
document.getElementById("btnThem").addEventListener("click", function () {
  document.getElementById("btnCapNhat").style.display = "none";
});
// Chuc nang them Nhan vien moi
document.getElementById("btnThemNV").addEventListener("click", function () {
  document.getElementById("btnCapNhat").style.display = "none";
  addStaff();
});
// Chuc nang cap nhat Nhan vien
document.getElementById("btnCapNhat").addEventListener("click", function () {
  var newStaff = getStaffInfor();
  var isValidStaff = validationUpdateStaff(newStaff);
  if (isValidStaff) {
    var editStaffIndex = getStaffIndex(MODE, newStaff.account, staffList);
    staffList[editStaffIndex] = newStaff;
    showStaff(staffList);
    saveLocalStorage(staffList);
    $("#myModal").modal("hide");
    document.getElementById("body-form").reset();
  }
});
function editStaff(account) {
  $("#myModal").modal("show");
  var editStaffIndex = getStaffIndex(MODE, account, staffList);
  showStaffOnModal(staffList[editStaffIndex]);
  document.getElementById("btnThemNV").style.display = "none";
  document.getElementById("tknv").readOnly = true;
}
// Chuc nang xoa Nhan vien
function deleteStaff(account) {
  var delStaffIndex = getStaffIndex(MODE, account, staffList);
  staffList.splice(delStaffIndex, 1);
  showStaff(staffList);
  saveLocalStorage(staffList);
}
// Chuc nang tim kiem Nhan vien theo xep loai
document.getElementById("searchName").addEventListener("input", function () {
  var staffRank = document.getElementById("searchName").value;
  if (!staffRank) {
    showStaff(staffList);
  } else {
    var rankList = searchStaff(MODE, staffRank, staffList);
    showStaff(rankList);
  }
});
