import React, { Component } from "react";

export default class ProductDetail extends Component {
  render() {
    return (
      <div className="row mt-5">
        <div className="col-md-4">
          <br />
          <h3 className="text-center">{this.props.productDetail.tenSP}</h3>
          <img
            className="card-img-top"
            src={this.props.productDetail.hinhAnh}
            width={170}
            height={300}
            alt={this.props.productDetail.tenSP}
          ></img>
        </div>
        <div className="col-md-8">
          <table className="table">
            <thead>
              <tr>
                <td colSpan={2}>
                  <h3>Thông số kỹ thuật</h3>
                </td>
              </tr>
              <tr>
                <td>Màn hình</td>
                <td>{this.props.productDetail.manHinh}</td>
              </tr>
              <tr>
                <td>Hệ điều hành</td>
                <td>{this.props.productDetail.heDieuHanh}</td>
              </tr>
              <tr>
                <td>Camera trước</td>
                <td>{this.props.productDetail.cameraTruoc}</td>
              </tr>
              <tr>
                <td>Camera sau</td>
                <td>{this.props.productDetail.cameraSau}</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>{this.props.productDetail.ram}</td>
              </tr>
              <tr>
                <td>ROM</td>
                <td>{this.props.productDetail.rom}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    );
  }
}
