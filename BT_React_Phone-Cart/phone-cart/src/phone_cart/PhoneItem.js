import React, { Component } from "react";

export default class PhoneItem extends Component {
  render() {
    return (
      <div className="col-4">
        <div className="card">
          <img
            className="card-img-top"
            src={this.props.data.hinhAnh}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{this.props.data.tenSP}</h5>
            <div className="d-flex justify-content-around">
              <button
                className="btn btn-success"
                onClick={() => {
                  this.props.showDetail(this.props.data.maSP);
                }}
              >
                Xem chi tiết
              </button>
              <button
                className="btn btn-danger"
                data-toggle="modal"
                data-target="#productModal"
                onClick={() => {
                  this.props.addToCart(this.props.data);
                }}
              >
                Thêm giỏ hàng
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
