import React, { Component } from "react";
import ProductList from "./ProductList";
import { productData } from "./ProductData";
import ProductDetail from "./ProductDetail";
import Cart from "./Cart";
export default class PhoneCart extends Component {
  state = {
    productData: productData,
    productDetail: [],
    cart: [],
  };
  showProductDetail = (maSP) => {
    let index = this.state.productData.findIndex((item) => {
      return item.maSP === maSP;
    });
    if (index !== -1) {
      this.setState({
        productDetail: productData[index],
      });
      document.getElementById("productDetail").setAttribute("class", "d-block");
    }
  };
  addToCart = (sanPham) => {
    let cartList = [...this.state.cart];
    let index = cartList.findIndex((item) => {
      return item.maSP === sanPham.maSP;
    });
    if (index === -1) {
      let newSP = { ...sanPham, soluong: 1 };
      cartList.push(newSP);
    } else {
      cartList[index].soluong++;
    }
    this.setState({
      cart: cartList,
    });
  };
  handleXoaSanPham = (idSanPham) => {
    let index = this.state.cart.findIndex((sanpham) => {
      return sanpham.maSP == idSanPham;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.cart];
      cloneGioHang.splice(index, 1);
      if (cloneGioHang.length == 0) {
        document.querySelector(".modal-backdrop").remove();
      }
      this.setState({ cart: cloneGioHang });
    }
  };
  handleTangGiamSL = (idSanPham, giaTri) => {
    let cloneGioHang = [...this.state.cart];
    let index = cloneGioHang.findIndex((sanpham) => {
      return sanpham.maSP === idSanPham;
    });
    if (index !== -1) {
      cloneGioHang[index].soluong += giaTri;
    }
    cloneGioHang[index].soluong == 0 && cloneGioHang.splice(index, 1);
    if (cloneGioHang.length == 0) {
      document.querySelector(".modal-backdrop").remove();
    }
    this.setState({
      cart: cloneGioHang,
    });
  };
  render() {
    return (
      <div>
        <ProductList
          productData={this.state.productData}
          showDetail={this.showProductDetail}
          addToCart={this.addToCart}
        />
        {this.state.cart.length > 0 && (
          <Cart
            handleXoaSanPham={this.handleXoaSanPham}
            handleTangGiamSL={this.handleTangGiamSL}
            cartList={this.state.cart}
          />
        )}
        <div id="productDetail" className="d-none">
          <ProductDetail productDetail={this.state.productDetail} />
        </div>
      </div>
    );
  }
}
