import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    return (
      <div
        className="modal fade"
        id="productModal"
        tabindex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-centered"
          style={{ maxWidth: "1000px" }}
        >
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Giỏ hàng
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <td>Mã sản phẩm</td>
                    <td>Hình ảnh</td>
                    <td>Tên sản phẩm</td>
                    <td>Số lượng</td>
                    <td>Đơn giá</td>
                    <td>Thành tiền</td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  {this.props.cartList.map((sanpham) => {
                    return (
                      <tr>
                        <td>{sanpham.maSP}</td>
                        <td>
                          <img src={sanpham.hinhAnh} width={100} />
                        </td>
                        <td>{sanpham.tenSP}</td>
                        <td>
                          <button
                            className="btn btn-primary mx-2"
                            onClick={() => {
                              this.props.handleTangGiamSL(sanpham.maSP, 1);
                            }}
                          >
                            +
                          </button>
                          {sanpham.soluong}
                          <button
                            className="btn btn-primary mx-2"
                            onClick={() => {
                              this.props.handleTangGiamSL(sanpham.maSP, -1);
                            }}
                          >
                            -
                          </button>
                        </td>
                        <td>{sanpham.giaBan}</td>
                        <td>{sanpham.giaBan * sanpham.soluong}</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => {
                              this.props.handleXoaSanPham(sanpham.maSP);
                            }}
                          >
                            Xoá
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Đóng
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
