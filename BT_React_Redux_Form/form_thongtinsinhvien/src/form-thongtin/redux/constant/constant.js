export const GET_INFO = "Get info";
export const ADD_USER = "Add user";
export const EDIT_USER = "Edit user";
export const DELETE_USER = "Delete user";
export const SHOW_MESSAGE = "Show message";
