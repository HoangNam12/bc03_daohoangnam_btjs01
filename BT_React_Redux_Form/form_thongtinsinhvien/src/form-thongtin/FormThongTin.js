import { Button, Form, Input } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_USER, GET_INFO, SHOW_MESSAGE } from "./redux/constant/constant";
class FormThongTin extends Component {
  render() {
    return (
      <div>
        <h1 className="mb-5 text-3xl text-white py-2 bg-black font-medium">
          Thông tin sinh viên
        </h1>
        <Form
          name="basic"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          autoComplete="off"
        >
          <div className="w-1/2 inline-block">
            <Form.Item label="Mã sinh viên" className="mb-2">
              <Input
                name="id"
                onChange={(event) => {
                  this.props.handleGetInfo(event);
                }}
                value={this.props.user.id}
              />
              <span
                className={
                  this.props.message.id ? "block text-red-500" : "hidden"
                }
              >
                {this.props.message.id}
              </span>
            </Form.Item>
            <Form.Item label="Tên" className="mb-2">
              <Input
                name="name"
                onChange={(event) => {
                  this.props.handleGetInfo(event);
                }}
                value={this.props.user.name}
              />
              <span
                className={
                  this.props.message.name ? "block text-red-500" : "hidden"
                }
              >
                {this.props.message.name}
              </span>
            </Form.Item>
          </div>
          <div className="w-1/2 inline-block">
            <Form.Item label="Số điện thoại" className="mb-2">
              <Input
                style={{ width: "100%" }}
                onChange={(event) => {
                  this.props.handleGetInfo(event);
                }}
                name="phone"
                value={this.props.user.phone}
              />
              <span
                className={
                  this.props.message.phone ? "block text-red-500" : "hidden"
                }
              >
                {this.props.message.phone}
              </span>
            </Form.Item>
            <Form.Item label="Email" className="mb-2">
              <Input
                onChange={(event) => {
                  this.props.handleGetInfo(event);
                }}
                name="email"
                value={this.props.user.email}
              />
              <span
                className={
                  this.props.message.email ? "block text-red-500" : "hidden"
                }
              >
                {this.props.message.email}
              </span>
            </Form.Item>
          </div>

          <div className="flex ml-40">
            <Button
              type="primary"
              htmlType="submit"
              className="text-blue-500"
              onClick={() => {
                this.props.handleAddUser();
                this.props.handleShowMessage();
              }}
            >
              Hoàn tất
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    message: state.formReducer.message,
    user: state.formReducer.userInfo,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleGetInfo: (item) => {
      dispatch({
        type: GET_INFO,
        payload: item,
      });
    },
    handleAddUser: () => {
      dispatch({
        type: ADD_USER,
        payload: "",
      });
    },
    handleShowMessage: () => {
      dispatch({
        type: SHOW_MESSAGE,
        payload: "",
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FormThongTin);
