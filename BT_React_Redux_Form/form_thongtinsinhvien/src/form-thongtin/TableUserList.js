import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal } from "antd";
import { DELETE_USER, EDIT_USER } from "./redux/constant/constant";

class TableUserList extends Component {
  state = {
    isModalVisible: false,
    userID: null,
  };

  showModal = () => {
    this.setIsModalVisible(true);
  };

  handleOk = () => {
    this.setIsModalVisible(false);
  };

  handleCancel = () => {
    this.setIsModalVisible(false);
  };
  setIsModalVisible = (value) => {
    this.setState({
      isModalVisible: value,
    });
  };
  render() {
    return (
      <table className="table table-bordered mt-3">
        <thead className="table-dark">
          <tr>
            <th>Mã sinh viên</th>
            <th>Họ tên</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Thao tác</th>
          </tr>
        </thead>
        <tbody>
          {this.props.userList.map((user, index) => {
            return (
              user.id &&
              user.name &&
              user.email && (
                <tr key={index}>
                  <td>{user.id}</td>
                  <td>{user.name}</td>
                  <td>{user.phone}</td>
                  <td>{user.email}</td>
                  <td>
                    <button
                      className="btn btn-success"
                      onClick={() => {
                        this.props.handleEditUser(user);
                      }}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-danger ml-2"
                      onClick={() => {
                        this.showModal();
                        this.setState({
                          userID: user.id,
                        });
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              )
            );
          })}
        </tbody>
        <Modal
          title="Warning"
          visible={this.state.isModalVisible}
          footer={[
            <button
              className="btn btn-danger"
              onClick={() => {
                this.handleOk();
                this.props.handleDeleteUser(this.state.userID);
              }}
            >
              ok
            </button>,
            <button
              className="btn btn-success ml-2"
              onClick={() => {
                this.handleCancel();
              }}
            >
              cancel
            </button>,
          ]}
        >
          <p className="font-medium">Want to delete this user?</p>
        </Modal>
      </table>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    userList: state.formReducer.userList,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleEditUser: (user) => {
      dispatch({
        type: EDIT_USER,
        payload: user,
      });
    },
    handleDeleteUser: (user) => {
      dispatch({
        type: DELETE_USER,
        payload: user,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TableUserList);
