import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            {this.props.productData.map((product) => {
              return (
                <ProductItem
                  product={product}
                  handleAddToCart={this.props.handleAddToCart}
                  handleProductDetail={this.props.handleProductDetail}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
