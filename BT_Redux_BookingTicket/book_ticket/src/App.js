import logo from "./logo.svg";
import "./App.css";
import BookTicket from "./book-ticket/BookTicket";

function App() {
  return (
    <div className="App">
      <BookTicket />
    </div>
  );
}

export default App;
