var arr = [];
function addNum() {
  var num = document.getElementById("num").value * 1;
  arr.push(num);
  document.getElementById("arr").value = arr;
  document.getElementById("num").value = "";
}
function clearNum() {
  document.getElementById("arr").value = "";
}
/**
 *
 * Bai 1:
 *
 */
function ketquabai1() {
  var sum = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      sum += arr[i];
    }
  }
  document.getElementById("sumOfPosNum").value = sum;
}
/**
 *
 * Bai 2:
 *
 */
function ketquabai2() {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      count++;
    }
  }
  document.getElementById("numOfPosNum").value = count;
}
/**
 *
 * Bai 3:
 *
 */
function ketquabai3() {
  var minNum = arr[0];
  for (var i = 1; i < arr.length; i++) {
    if (arr[i] < minNum) {
      minNum = arr[i];
    }
  }
  document.getElementById("minOfArr").value = minNum;
}

/**
 *
 * Bai 4:
 *
 */
function ketquabai4() {
  var minPosNum = arr[0];
  for (var i = 1; i < arr.length; i++) {
    if (minPosNum > 0) {
      if (arr[i] > 0 && arr[i] < minPosNum) {
        minPosNum = arr[i];
      }
    } else {
      minPosNum = arr[i];
    }
  }
  document.getElementById("minOfPosNum").value = minPosNum;
}
/**
 *
 * Bai 5:
 *
 */
function ketquabai5() {
  var evenNum = 0;
  evenNum = arr[arr.indexOf(checkEvenNum())];
  document.getElementById("evenLastNum").value = evenNum;
}
function checkEvenNum() {
  for (var i = arr.length - 1; i > 0; i--) {
    if (arr[i] % 2 == 0) {
      return arr[i];
    }
  }
  return false;
}
/**
 *
 * Bai 6:
 *
 */
function ketquabai6() {
  var newArr = [...arr];
  var firstNum = document.getElementById("num1").value * 1;
  var secondNum = document.getElementById("num2").value * 1;
  swapArr(newArr, newArr.indexOf(firstNum), newArr.indexOf(secondNum));
  document.getElementById("newArr").value = newArr;
}
function swapArr(arr, a, b) {
  var temp = arr[a];
  arr[a] = arr[b];
  arr[b] = temp;
}
/**
 *
 * Bai 7:
 *
 */
function ketquabai7() {
  var newArr2 = [...arr];
  newArr2.sort(function (a, b) {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  });
  document.getElementById("sortIncr").value = newArr2;
}
/**
 *
 * Bai 8:
 *
 */
function ketquabai8() {
  var primeNum = arr[arr.findIndex(isPrime)];
  document.getElementById("soNguyenTo").value = primeNum;
}
function isPrime(number) {
  for (var i = 2; i < number; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return number > 1;
}
/**
 *
 * Bai 9:
 *
 */
var realArr = [];
function addRealNum() {
  var realNum = document.getElementById("realNum").value * 1;
  realArr.push(realNum);
  document.getElementById("realArr").value = realArr;
  document.getElementById("realNum").value = "";
}
function ketquabai9() {
  var realNumCount = 0;
  realArr.forEach((element) => {
    if (Number.isInteger(element)) {
      realNumCount++;
    }
  });
  document.getElementById("intNum").value = realNumCount;
}
/**
 *
 * Bai 10:
 *
 */
var contentHTML = "";
function ketquabai10() {
  var posNumCount = 0;
  var negNumCount = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      posNumCount++;
    }
  }
  negNumCount = arr.length - posNumCount;
  if (negNumCount < posNumCount) {
    contentHTML = `Âm tiêu dương trưởng`;
  } else if (negNumCount > posNumCount) {
    contentHTML = `Âm thịnh dương suy`;
  } else {
    contentHTML = `Âm dương cân bằng`;
  }
  document.getElementById("comPosNumNegNum").value = contentHTML;
}
