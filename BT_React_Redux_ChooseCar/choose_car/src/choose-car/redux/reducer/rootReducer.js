import { combineReducers } from "redux";
import { chooseCarReducer } from "./chooseCarReducer";

export const rootReducer = combineReducers({
  chooseCarReducer,
});
