import { CarDetail } from "../../CarDetailData";
import { CHANGE_COLOR } from "../constant/constants";
let initialState = {
  carUrl: "",
  carDetail: CarDetail,
  carInfo: [],
};

export const chooseCarReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_COLOR: {
      let cloneCar = [...state.carDetail];
      let index = cloneCar.findIndex((car) => {
        return car.id === payload.id;
      });
      state.carInfo = { ...cloneCar[index] };
      state.carUrl = cloneCar[index].srcImg;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
