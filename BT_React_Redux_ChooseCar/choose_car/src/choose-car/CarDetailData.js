export const CarDetail = [
  {
    id: 1,
    title: "Crystal Black",
    type: "Pearl",
    img: "./image/icons/black-icon.jpg",
    srcImg: "./image/products/black-car.jpg",
    color: "Black",
    price: "19,550",
    engineType: "In-Line 4-Cylinder",
    displacement: "1996 cc",
    horsepower: "158 @ 6500 rpm",
    torque: "138 lb-ft @ 4200 rpm",
    redline: "6700 rpm",
    wheels: [
      {
        idWheel: 1,
        srcImg: "images-black/images-black-1/",
      },
      {
        idWheel: 2,
        srcImg: "images-black/images-black-2/",
      },
      {
        idWheel: 3,
        srcImg: "images-black/images-black-3/",
      },
    ],
  },
  {
    id: 2,
    title: "Modern Steel",
    type: "Metallic",
    img: "./image/icons/steel-icon.jpg",
    srcImg: "./image/products/steel-car.jpg",
    color: "Steel",
    price: "20,550",
    engineType: "In-Line 4-Cylinder",
    displacement: "1996 cc",
    horsepower: "158 @ 6500 rpm",
    torque: "138 lb-ft @ 4200 rpm",
    redline: "6700 rpm",
    wheels: [
      {
        idWheel: 1,
        srcImg: "images-steel/images-steel-1/",
      },
      {
        idWheel: 2,
        srcImg: "images-steel/images-steel-2/",
      },
      {
        idWheel: 3,
        srcImg: "images-steel/images-steel-3/",
      },
    ],
  },
  {
    id: 3,
    title: "Lunar Silver",
    type: "Metallic",
    img: "./image/icons/silver-icon.jpg",
    srcImg: "./image/products/silver-car.jpg",
    color: "Silver",
    price: "21,550",
    engineType: "In-Line 4-Cylinder",
    displacement: "1996 cc",
    horsepower: "158 @ 6500 rpm",
    torque: "138 lb-ft @ 4200 rpm",
    redline: "6700 rpm",
    wheels: [
      {
        idWheel: 1,
        srcImg: "images-silver/images-silver-1/",
      },
      {
        idWheel: 2,
        srcImg: "images-silver/images-silver-2/",
      },
      {
        idWheel: 3,
        srcImg: "images-silver/images-silver-3/",
      },
    ],
  },
  {
    id: 4,
    title: "Rallye Red",
    type: "Metallic",
    img: "./image/icons/red-icon.jpg",
    srcImg: "./image/products/red-car.jpg",
    color: "Red",
    price: "22,550",
    engineType: "In-Line 4-Cylinder",
    displacement: "1996 cc",
    horsepower: "158 @ 6500 rpm",
    torque: "138 lb-ft @ 4200 rpm",
    redline: "6700 rpm",
    wheels: [
      {
        idWheel: 1,
        srcImg: "images-red/images-red-1/",
      },
      {
        idWheel: 2,
        srcImg: "images-red/images-red-2/",
      },
      {
        idWheel: 3,
        srcImg: "images-red/images-red-3/",
      },
    ],
  },
];
