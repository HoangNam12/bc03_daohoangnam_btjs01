import React, { Component } from "react";
import { connect } from "react-redux";
import CarDetail from "./CarDetail";
import IconColor from "./IconColor";

class ChooseCar extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <section className="col-8 mt-3 px-0">
            <div>
              <img
                src={this.props.carUrl}
                style={{ width: "100%", height: "554px" }}
              ></img>
            </div>
            <CarDetail />
          </section>
          <section className="col-4 px-0">
            <div>
              <table className="table table-bordered mt-3">
                <thead className="table-dark">
                  <tr>
                    <td>Exterior Color</td>
                  </tr>
                </thead>
                <IconColor />
              </table>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    carUrl: state.chooseCarReducer.carUrl,
  };
};
export default connect(mapStateToProps)(ChooseCar);
