import React from "react";

export default function Cart({ cart, handleXoaSanPham, handleSoLuong }) {
  return (
    <div
      className="modal fade"
      id="productModal"
      tabindex="-1"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div
        className="modal-dialog modal-dialog-centered"
        style={{ maxWidth: "1000px" }}
      >
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Giỏ hàng
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <table className="table">
              <thead>
                <tr>
                  <td>Mã sản phẩm</td>
                  <td>Hình ảnh</td>
                  <td>Tên sản phẩm</td>
                  <td>Số lượng</td>
                  <td>Đơn giá</td>
                  <td>Thành tiền</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {cart.map((sanpham) => {
                  return (
                    <tr>
                      <td>{sanpham.maSP}</td>
                      <td>
                        <img src={sanpham.hinhAnh} width={100} />
                      </td>
                      <td>{sanpham.tenSP}</td>
                      <td>
                        <button
                          className="btn btn-primary mx-2"
                          onClick={() => {
                            handleSoLuong(sanpham, 1);
                          }}
                        >
                          +
                        </button>
                        {sanpham.number}
                        <button
                          className="btn btn-primary mx-2"
                          onClick={() => {
                            handleSoLuong(sanpham, -1);
                          }}
                        >
                          -
                        </button>
                      </td>
                      <td>{sanpham.giaBan}</td>
                      <td>{sanpham.giaBan * sanpham.number}</td>
                      <td>
                        <button
                          className="btn btn-danger"
                          onClick={() => {
                            handleXoaSanPham(sanpham);
                          }}
                        >
                          Xoá
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              className="btn btn-primary text-blue-700 hover:text-white"
              data-dismiss="modal"
            >
              Đóng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
