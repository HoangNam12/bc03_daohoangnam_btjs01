import React from "react";

export default function PhoneItem({ data, handleAddToCart, showDetail }) {
  return (
    <div className="col-4">
      <div className="card">
        <img className="card-img-top" src={data.hinhAnh} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{data.tenSP}</h5>
          <div className="d-flex justify-content-around">
            <button
              className="btn btn-success"
              onClick={() => {
                showDetail(data);
              }}
            >
              Xem chi tiết
            </button>
            <button
              className="btn btn-danger"
              data-toggle="modal"
              data-target="#productModal"
              onClick={() => {
                handleAddToCart(data);
              }}
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
