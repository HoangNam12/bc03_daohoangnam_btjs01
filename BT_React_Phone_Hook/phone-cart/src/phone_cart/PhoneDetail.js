import React from "react";

export default function PhoneDetail({ productDetail }) {
  return (
    <div>
      <div className="row mt-5">
        <div className="col-md-4">
          <br />
          <h3 className="text-center">{productDetail.tenSP}</h3>
          <img
            className="card-img-top h-65"
            src={productDetail.hinhAnh}
            width={170}
            height={500}
            alt={productDetail.tenSP}
          ></img>
        </div>
        <div className="col-md-8">
          <table className="table">
            <thead>
              <tr>
                <td colSpan={2}>
                  <h3>Thông số kỹ thuật</h3>
                </td>
              </tr>
              <tr>
                <td>Màn hình</td>
                <td>{productDetail.manHinh}</td>
              </tr>
              <tr>
                <td>Hệ điều hành</td>
                <td>{productDetail.heDieuHanh}</td>
              </tr>
              <tr>
                <td>Camera trước</td>
                <td>{productDetail.cameraTruoc}</td>
              </tr>
              <tr>
                <td>Camera sau</td>
                <td>{productDetail.cameraSau}</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>{productDetail.ram}</td>
              </tr>
              <tr>
                <td>ROM</td>
                <td>{productDetail.rom}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  );
}
