import React, { useState } from "react";
import Cart from "./Cart";
import PhoneDetail from "./PhoneDetail";
import PhoneList from "./PhoneList";
import { ProductData } from "./PhoneData";
export default function PhoneCart() {
  let productData = ProductData;
  const [cartList, setCartList] = useState([]);
  const [productDetail, setProductDetail] = useState([]);
  const getProductIndex = (product, productList) => {
    let index = productList.findIndex((item) => {
      return item.maSP === product.maSP;
    });
    return index;
  };
  let handleAddToCart = (product) => {
    let cloneCart = [...cartList];
    let index = getProductIndex(product, cloneCart);
    if (index === -1) {
      let newProduct = { ...product, number: 1 };
      cloneCart.push(newProduct);
      setCartList(cloneCart);
    } else {
      cloneCart[index].number++;
      setCartList(cloneCart);
    }
  };
  let handleXoaSanPham = (product) => {
    let cloneCart = [...cartList];
    let index = getProductIndex(product, cloneCart);
    cloneCart.splice(index, 1);
    setCartList(cloneCart);
  };
  let handleSoLuong = (product, type) => {
    let cloneCart = [...cartList];
    let index = getProductIndex(product, cloneCart);
    if (cloneCart[index].number === 1 && type === -1) {
      cloneCart.splice(index, 1);
      setCartList(cloneCart);
    } else {
      cloneCart[index].number += type;
      setCartList(cloneCart);
    }
  };
  let handleShowDetail = (product) => {
    setProductDetail(product);
  };
  return (
    <div>
      <PhoneList
        productData={productData}
        handleAddToCart={handleAddToCart}
        showDetail={handleShowDetail}
      />
      <Cart
        cart={cartList}
        handleXoaSanPham={handleXoaSanPham}
        handleSoLuong={handleSoLuong}
      />
      <PhoneDetail productDetail={productDetail} />
    </div>
  );
}
